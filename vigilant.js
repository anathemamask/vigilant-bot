//Define necessary requirements
const Discord = require('discord.js')
const client = new Discord.Client()
const mysql = require('mysql');
const fs = require('fs');
//Implement NWOD Diceroller from Lady Luck Bot by Stranjer
//Initialize globals for Seeding system
//Define SQL connection
const con = mysql.createConnection({
    host: "example.example.org",
    database: "vigilant",
    user: "SQLusernamewithdatabaseaccess",
    password: "yourSQLpassword",
})
//If error on con.connect throw the error to console, otherwise announce connection!
con.connect(err => {
    if (err) throw err;
    console.log("SQL Connected!");
});

//Initialization and wakeup messages
client.on('ready', () => {
    // Set Status, this follows the discord.js Status API. Type can be Watching, Playing, or Streaming
    client.user.setActivity("for criminal misuse of magic", {
        type: "WATCHING"
    })

    // list servers the bot is connected to
    console.log("Servers:")
    client.guilds.forEach((guild) => {
        console.log(" - " + guild.name)
        // list all channels on those servers
        guild.channels.forEach((channel) => {
            console.log(` -- ${channel.name} (${channel.type}) - ${channel.id}`)
        })
    })

    client.on('message', (receivedMessage) => {
        if (receivedMessage.author == client.user) { //Check if the bot's messaging itself, if so, don't respond. NO LOOPS HERE.
            return
        }
        if (receivedMessage) { // on receiving a message, run the logging process
            processLog(receivedMessage)
        }
        //establish all the necessary variables for logging
        function processLog(receivedMessage) {
            let msg = receivedMessage
            let logDate = msg.createdAt.toString()
            let splitDate = logDate.split(' ')
            let month = splitDate[1]
            let day = splitDate[2]
            let year = splitDate[3]
            let time = splitDate[4]
            let messageRaw = msg.content
            let attached = (receivedMessage.attachments).array()
            //If the message has an attachment, get the URL and log that too.
            if (attached.length < 1) { 
                var attachurl = ""
            }
            let messageLogged = messageRaw + attachurl //Combine the attachment url to the message content for logging
            //uncomment line 63 to log it to the console as well
            //console.log("Message Received: " + '#' + receivedMessage.channel.name + ' ' + msg.author.tag + ' ' + msg.createdAt + ' <' + msg.author.username + '> ' + msg + attachurl) //Log to the console, comment this line out to disable it if it's clogging up your log.
            //check if the table exists for the channel name, if not, create it with the columns, all in TEXT, because lazy
            con.query("CREATE TABLE IF NOT EXISTS `" + receivedMessage.channel.name + "` (userID TEXT, year TEXT, month TEXT, day TEXT, time TEXT, nick TEXT, message TEXT);")
            //write each logged message into the database under the correct table
            con.query("INSERT INTO`" + receivedMessage.channel.name + "` (userID, year, month, day, time, nick, message) VALUES (" +
                mysql.escape(msg.author.tag) + "," + mysql.escape(year) + "," + mysql.escape(month) + "," + mysql.escape(day) + "," +
                mysql.escape(time) + "," + mysql.escape(msg.author.username) + "," + mysql.escape(messageLogged) + ');'
            )
        }
        //Define the command delimiter
        if (receivedMessage.content.startsWith("!")) {
            processCommand(receivedMessage)
        }
        //Breakdown the command received into managable pieces
        function processCommand(receivedMessage, guild, Discord, Client) {
            let fullCommand = receivedMessage.content.substr(1) // Removes the leading exclaim
            let splitCommand = fullCommand.split(" ") // splits the message up in pieces for each space
            let primaryCommand = splitCommand[0] // first word right after the exclamation mark becomes the command
            chanString = splitCommand[1]
            let arguments = splitCommand.slice(1) // all other words become the arguments

            console.log("Command received: " + primaryCommand) //Posts to console commands received for logging purposes
            console.log("arguments: " + arguments) //separate in case there are no arguments for that command

            //List of command triggers to watch for, and what function to pull when activated
            //        if (primaryCommand == "help") {
            //            helpCommand(arguments, receivedMessage)
            //        }
            if (primaryCommand == "resolvename") {
                resolveName(receivedMessage)
            }
            if (primaryCommand == "getseed") {
                getSeed(receivedMessage)
            }
            if (primaryCommand == "reseed") {
                reSeed(receivedMessage)
            }
            if (primaryCommand == "getlog") { //Process the date of the command [syntax is month(alphabetical, 3 chars)-day-year, ex mar-12-2019] and pull the SQL log for that channel for 24 hours
                //Change "logaccess" to whatever role you want to grant access to the !getlog command.
                let allowedRole = receivedMessage.guild.roles.find(role => role.name === "Logaccess")
                if (receivedMessage.member.roles.has(allowedRole.id)) { //check to make sure the user's in the role authorization 
                    logCommand(arguments, receivedMessage)
                } else {
                    receivedMessage.channel.send("Sorry, you are unauthorized to access system logs.")
                }
            }

            function resolveName() {
                chanStringraw = splitCommand[1]
                chanString = chanStringraw.replace("#", "").replace("<", "").replace(">", "")
//                console.log(chanString)
                channelData = receivedMessage.guild.channels.get(chanString)
                resolvedName = channelData.name
//                console.log(resolvedName)
                //if(msg.author.voiceChannel == "afkChannelId"){
                receivedMessage.channel.send(chanString + " Resolves to: " + resolvedName)
            }


            /* Help system formatting
                function helpCommand(arguments, receivedMessage) {
                        if (arguments.length > 0) {
                            receivedMessage.channel.send("Displaying help for " + arguments) //If message is longer than 0, read the arguments and display the help listing
                        } else {
                            receivedMessage.channel.send("I am not sure I understand your request. Try `!help [topic]`")
                        }
                    }
            */
            function logCommand(err, res, msg, guild) { //log retriever
                if (arguments.length < 0) {
                    //If message is longer than 0, read the arguments and display the help listing
                    receivedMessage.channel.send("Sorry, an example of the the correct format of the command is: !getlog <#channel> <mon-01-2019>, or to get the current channel use !getlog <date> instead")
                } else {

                    //Defining the vars for ripping the log out of the database
//                    console.log(chanString)
                    //Check to see if the user used !getlog <channelname> <date>, if so, resolve the channel for SQL
                    if (splitCommand[1].includes("<#")) {
                        chanStringraw = splitCommand[1]
                        chanString = chanStringraw.replace("#", "").replace("<", "").replace(">", "")
//                        console.log(chanString)
                        channelData = receivedMessage.guild.channels.get(chanString)
                        resolvedName = channelData.name
//                        console.log(resolvedName)

                        loggerDate = splitCommand[2].split("-")
                        channelName = resolvedName
//                        console.log("channel name is:" + channelName)
                    } else {
                        //If they just used !getlog <date> grab the log of the current channel.
                        var loggerDate = splitCommand[1].split("-") //split the date up into its component parts
                        channelName = receivedMessage.channel.name
                    }
                    let getMonth = loggerDate[0]
                    let getDay = loggerDate[1]
                    let getYear = loggerDate[2]
                    let logFile = (channelName + "-" + getMonth + "." + getDay + "." + getYear + ".txt")

                    /*  debug script to make sure the date is parsed properly
                                    console.log("Month is " + getMonth)
                                    console.log("Day is " + getDay)
                                    console.log("Year is " + getYear)
                                    console.log("Logfile name is " +logFile)
                    */
                    //Subfunction for reading the SQL database
                    con.connect(function (err) {
                        con.query('SELECT time, nick, message FROM ' +
                            '`' + channelName + '`' +
                            ' WHERE (year = ' + getYear +
                            ` AND month = '` + getMonth +
                            `' AND day = ` + getDay + ');',
                            //Subfunction to create the file.
                            function (err, result, fields) {
                                //Check if the file already exists, 
                                //if so, delete it to make room for the new generated file, not good housekeeping but it works
                                if (fs.existsSync("logs/" + logFile)) {
                                    (fs.unlinkSync("logs/" + logFile));
                                } else if (err) throw err; {
                                    fs.writeFileSync("logs/" + logFile,
                                        ("~~~Open Session " + getMonth + " " + getDay + " " + getYear + "~~~" + "\r\n"))
                                    //Write the results into a JS object map
                                    Object.keys(result).forEach(function (key) {
                                        var row = result[key];
                                        //Dump the object map into the log file, formatted similar to IRC output
                                        fs.appendFileSync("logs/" + logFile,
                                            ("[" + row.time + "]" + " <" + row.nick + "> " + row.message + "\r\n"))
                                    })
                                    //Post the log to the channel from which the !getlog command was received.
                                    receivedMessage.channel.send("Here is the log of " + channelName +
                                        " from " + getMonth +
                                        " " + getDay +
                                        " " + getYear, {
                                            files: ["logs/" + logFile]
                                        });
                                }
                            });
                    });
                }

            }
        }
    })


})

// Get your bot's secret token from:
// https://discordapp.com/developers/applications/
// Click on your application -> Bot -> Token -> "Click to Reveal Token"        

bot_secret_token = "yourbotsecrettoken"

client.login(bot_secret_token)